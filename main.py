
#==============================================================================
from flask import Flask
import socket

app = Flask(__name__)

@app.route("/")
def home():
      rollnum="i19-1250"
      return  "Hostname = "+socket.gethostname()+"  "+"Roll No= "+rollnum

if __name__ == "__main__":
    app.run(host="0.0.0.0",port=5000,debug=True)
